---
authors:
- admin
bio: Gjalt-Jorn Peters works at the Dutch Open University, where he teaches methodology and statistics, and does research into health psychology, specifically behavior change, in general and applied to nightlife-related risk behavior. He is involved in Dutch nightlife prevention project Celebrate Safe, where he is responsible for the Party Panel study. In addition, he develops and maintains a number of R packages.
email: ""
interests:
- Behavior Change
- Intervention Mapping
name: Gjalt-Jorn Peters
organizations:
- name: Open University of the Netherlands
  url: https://ou.nl
role: Assistant professor in methodology, statistics, and health psychology
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/matherion
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=Qw8JJh0AAAAJ
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/matherion
superuser: true
user_groups:
- Researchers
- Visitors
---

Gjalt-Jorn Peters was born in Sneek, Fryslân, the Netherlands, European Union, at the 26th of October 1981. He currently lives in Maastricht, Limburg, the Netherlands, European Union.

<!-- After a not-so-successful attempts to study computer science in Enschede, the Netherlands, he started studying psychology in Maastricht, the Netherlands. That worked out better, and he continued to obtain his PhD studying the determinants of ecstasy use and related harm reduction strategies. -->




