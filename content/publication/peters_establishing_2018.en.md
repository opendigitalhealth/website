---
abstract: ""
abstract_short: ""
authors:
- Gjalt-Jorn Ygram Peters
- Rik Crutzen
date: 2018-01-01T00:00:00
image_preview: ""
projects: []
publication: European Health Psychologist, (20), 3, _pp. 485--495_, https://doi.org/10.31234/osf.io/5wjy4
publication_short: European Health Psychologist, (20), 3, _pp. 485--495_, https://doi.org/10.31234/osf.io/5wjy4
publication_types:
- "2"
selected: false
tags: []
title: 'Establishing determinant importance using CIBER: an introduction and tutorial'
url_code: ""
url_dataset: ""
url_pdf: ""
url_poster: ""
url_preprint: ""
url_project: ""
url_slides: ""
url_source: ""
url_video: ""
---

