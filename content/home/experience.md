---
active: false
date_format: January 2006
experience:
- company: GenCoin
  company_url: ""
  date_end: ""
  date_start: "2017-01-01"
  description: "\r\n  Responsibilities include:\r\n  \r\n  * Analysing\r\n  * Modelling\r\n  * Deploying\r\n  "
  location: California
  title: CEO
- company: University X
  company_url: ""
  date_end: "2016-12-31"
  date_start: "2016-01-01"
  description: Taught electronic engineering and researched semiconductor physics.
  location: California
  title: Professor
subtitle: ""
title: Experience
weight: 40
widget: experience
---
