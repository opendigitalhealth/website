---
active: true
advanced:
  css_class: ""
  css_style: 'padding-top: 20px; padding-bottom: 20px;'
design:
  background:
    color: white
    text_color_light: false
  columns: "1"
subtitle: ""
title: Story
weight: 100
widget: blank
---

## A story about evidence-based tools that die too early...

Here is a story (and you may have heard a very similar one before): A group of researchers in the UK gets a funding grant to develop an app. Their aim is to promote physical activity in older people. They outsource a company to code the app. They review literature, design the app and test it with the users. They run a study with 150 people who use the app and with 150 who don’t, and they show that this app was somehow effective. After a year, they publish an article and they put the app aside. It doesn’t get much publicity or downloads, doesn’t get updated and it dies after the funding period. Sad times. Does this sound familiar? 

## An alternative ending: Open, Transparent and Shared Digital Health

And here is an alternative ending to the story you just heard: The same group of researchers is keen to share their work. They have the codes for the app, the content and all anonymised user data they’ve gathered. They don’t have time or money to take it forward but they list the descriptions of the app, code, content and data gathered on the Open Digital Health platform where other users can see it.

A group of researchers in Spain wants to promote physical activity in older people. They browse the Open Digital Health platform and locate the app created by the first group. They get in touch with the authors and ask for the permission to adapt the app considering [appropriate licensing](https://osf.io/t3kp2/wiki/Licenses/). They get it granted, translate the app to Spanish, use it with 300 people, get feedback, modify it, test it, and then show that the new app is even more effective than the original version. They publish the results, acknowledge the original authors and list the information about the app back on the Open Digital Health platform together with new translated content.  Then a group of researchers in Chile finds the app on the platform and the story goes on...

## Tell us what do you think!

We are a group of health researchers passionate about digital health and about making it more accessible for all. Sharing digital health tools will provide cost-effective opportunities for faster breakthroughs. We are asking you to [fill in the survey](https://rsearch.eu/ls3/234982) (<2 minutes) to let us know if you want to join or give us your feedback. Or simply leave us your details in our [contact](#contact) form we will keep you posted.

