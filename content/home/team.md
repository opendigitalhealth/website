---
active: true
advanced:
  css_class: ""
  css_style: 'padding-top: 20px; padding-bottom: 20px;'
design:
  background:
    color: white
    text_color_light: false
  columns: "1"
subtitle: ""
title: Team
weight: 110
widget: blank
---

## Meet People Behind the Open Digital Health Initiative

This is a non-for-profit initiative. We want to encourage you to share evidence-based digital health tools. We want to create a searchable database of digital health tools, apps, websites, devices, to allow digital health to grow faster, be cheaper and more transparent across the countries.

## The Creators

### Dominika Kwasnicka

<img class="creator-img" src="img/kwasnicka-dominika.jpg" />

> Open Digital Health is a platform for scientists and developers to connect and to take their ideas forward.

### Robbert Sanderman

<img class="supporter-img" src="img/sanderman-robbert.jpg" />

> Because none of us wants to spend three or four years working on a health app that will never be downloaded.

### Rik Crutzen

<img class="supporter-img" src="img/crutzen-rik.jpg" />

> Because too much tax payers' money gets wasted on promising digital health interventions that die too early after funding grants finish.

### Gjalt Jorn Peters

<img class="supporter-img" src="img/peters-gjalt-jorn.jpg" />

> Because we are living in a connected world and Open Connected Science is a future of digital health.

### Gill ten Hoor

<img class="supporter-img" src="img/ten-hoor-gill.jpg" />

> Open Digital Health is all about sharing ideas,  content, data - whatever you feel comfortable sharing with other folk who may learn from you.

## Organisations Supporting Our Initiative

### EHPS



We are grateful to organisations and individuals supporting the Open Digital Health Initiative. Without your time, commitment and enthusiasm we will not be able to build this platform. We want to connect people who work in digital health space. Societies that bring us together are crucial to our success. **Thank you for being part of the Open Digital Health movement!**

