---
active: true
cta:
  icon: comments
  icon_pack: fas
  label: Join us!
  url: '#contact'
design:
  background:
    color: white
    text_color_light: true
hero_media: odhi-logo.png
title: OPEN DIGITAL HEALTH
weight: 10
widget: hero
---

<span style="color:#002f5eff">

Our mission is to share evidence-based digital health tools.

</span>
<!-- span style="text-shadow: none;"><a class="github-button" href="https://github.com/gcushen/hugo-academic" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star this on GitHub">Star</a><script async defer src="https://buttons.github.io/buttons.js"></script></span -->
