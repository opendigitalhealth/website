---
active: true
gallery_item:
- album: supporters
  caption: '**Sherry Pagoto** Professor, University of Connecticut, Director of the UConn Center for mHealth & Social Media, the USA'
  image: pagoto-sherry.jpg
- album: supporters
  caption: '**Ralf Schwarzer** Professor, Freie Universität Berlin, Germany, and SWPS University of Social Sciences and Humanities, Wroclaw, Poland'
  image: schwarzer-ralf.jpg
- album: supporters
  caption: '**Martin Hagger** Professor, Curtin University, Australia and University of Jyväskylä, TEKES, the Finnish Funding Agency for Innovation, Finland'
  image: hagger-martin.jpg
- album: supporters
  caption: '**Elaine Toomey** PhD, National University of Ireland, Galway and Cochrane, Ireland'
  image: toomey-elaine.jpg
- album: supporters
  caption: '**Dani Arigo** PhD, Rowan University, the USA'
  image: arigo-dani.jpg
- album: supporters
  caption: '**Amanda Rebar** PhD, Central Queensland University, Australia'
  image: rebar-amanda.jpg
subtitle: ""
title: Initiative suporters
weight: 30
widget: blank
---

<div style="width=200px;min-width:200px;float:left;">
<div class="supporter-text"><strong>Sherry Pagoto</strong> Professor, University of Connecticut, Director of the UConn Center for mHealth & Social Media, the USA</div>
<div class="supporter-text"><strong>Ralf Schwarzer</strong> Professor, Freie Universität Berlin, Germany, and SWPS University of Social Sciences and Humanities, Wroclaw, Poland</div>
<div class="supporter-text"><strong>Martin Hagger</strong> Professor, Curtin University, Australia and University of Jyväskylä, TEKES, the Finnish Funding Agency for Innovation, Finland</div>
<div class="supporter-text"><strong>Elaine Toomey</strong> PhD, National University of Ireland, Galway and Cochrane, Ireland</div>
<div class="supporter-text"><strong>Dani Arigo</strong> PhD, Rowan University, the USA</div>
<div class="supporter-text"><strong>Amanda Rebar</strong> PhD, Central Queensland University, Australia</div>
</div>

<div style="width=610px;min-width:610px;float:right;">

<img class="supporter-img" src="img/pagoto-sherry.jpg" />
<img class="supporter-img" src="img/schwarzer-ralf.jpg" />
<img class="supporter-img" src="img/hagger-martin.jpg" />
<img class="supporter-img" src="img/toomey-elaine.jpg" />
<img class="supporter-img" src="img/arigo-dani.jpg" />
<img class="supporter-img" src="img/rebar-amanda.jpg" />

</div>


<!--- {{< gallery album="supporters" >}} -->
