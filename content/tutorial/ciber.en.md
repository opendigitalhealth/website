---
date: "2019-03-19T00:00:00+01:00"
draft: false
linktitle: CIBER
menu:
  tutorial:
    parent: Overview
    weight: 2
title: Confidence Interval-Based Estimation of Relevance
toc: true
type: docs
---

To get started with Confidence Interval-Based Estimation of Relevance (CIBER), we have:

- [The article in Frontiers in Public Health](https://doi.org/10.3389/fpubh.2017.00165)
- [The article in the European Health Psychologist](https://doi.org/10.31234/osf.io/5wjy4)

