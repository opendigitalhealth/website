---
date: "2019-03-19T00:00:00+01:00"
draft: false
linktitle: ABCD
menu:
  tutorial:
    parent: Overview
    weight: 2
title: Acyclic Behavior Change Diagrams
toc: true
type: docs
---

For getting started with Acyclic Behavior Change Diagrams, we have:

- [The English vignette](https://r-packages.gitlab.io/behaviorchange/articles/abcd.html)
- [A Dutch introduction](https://r-packages.gitlab.io/behaviorchange/articles/abcd-laagdrempelige_nederlandse_uitleg.html)
- [A Dutch blog post geared towards practical use](https://sciencer.netlify.com/2019/02/een-voorbeeld-van-de-abcd-in-de-dagelijkse-praktijk-van-interventie-ontwikkeling/)

